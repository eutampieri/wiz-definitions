# Wiz smart home definitions

These files contain useful metadata to interface with Wiz products.

The light bulb definitions are based on [pyizlight](https://github.com/sbidy/pywizlight)
